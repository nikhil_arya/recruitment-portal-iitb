<!DOCTYPE html>
<%@page import="java.util.Iterator"%>
<%@page import="internships.Application"%>
<%@page import="sun.security.util.PendingException"%>
<%@ page import="database.Query"%>
<%@ page import="internships.Internship"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import ="java.sql.*" %>
<%@page import="database.Recruiter"%>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Recruiter | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <!-- Daterange picker -->
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

  <!-- fullCalendar 2.2.5-->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<script>
  function detail(id){
	  window.location="internship-detail?index="+id;
  }
  function editdetail(id){
	  window.location="edit-internship?index="+id;
  }
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini" data-root="<%out.print(getServletContext().getRealPath("/")+"uploads\\");%>">
<div class="wrapper">

  <jsp:include page="recruiter-sidebar.jsp"></jsp:include>
 <!-- Button trigger modal -->


<!-- Modal -->

<div class="modal fade" id="videocall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id="videocall" class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header " style="background-color:green">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color:white">Videocall Screening</h4>
      </div>
      <div class="modal-body">
      <div class="box-body" >
              <!-- the events -->
              <div class="row">
             
        <div id="hangout">     <div class="g-hangout" data-render="createhangout" id="videocall_hangout"> </div></div>
      <div class="col-lg-12" >
              <!-- the events -->
                <section class="content col-lg-6">
               <h4><a class="glyphicon glyphicon-phone" href="" id="videocall_contact"></a></h4>
          
      <h2 style="color:red" id="videocall_username"></h2>
           <button title="Resume" type="button" onclick="window.open(this.href,'popUpWindow','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;" id="videocall_resume"><i class="fa fa-file-pdf-o" >&nbsp;Resume</i></button>
        
             <h4 ><span id="videocall_education"></span>&nbsp;<span id="videocall_university"></span></h4> 
            <h4><span id="videocall_email"></span></h4> 
          <div class="box box-warning" style="overflow-x:auto">
              
            <!-- /.box-header -->
            <div class="box-body" >
              
                <!-- text input -->
                
                <table  >
                
                 <tr><th></th><th >Easy</th><th>Medium</th><th>Difficult</th></tr> 
                 <tr><th>Algorithms</th>
                 <td><input id="algo_easy" type="number" min="0" max="5" class="knob"  data-width="90" data-height="90" data-fgColor="#00a65a"></td>
                 <td><input id="algo_medium" type="number" min="0" max="5" class="knob"  data-width="90" data-height="90" data-fgColor="#f39c12"></td>
                 <td><input id="algo_hard" type="number" min="0" max="5" class="knob"  data-width="90" data-height="90" data-fgColor="#f56954"></td>
                 </tr>
                 
                 <tr><th></th><th >Easy</th><th>Medium</th><th>Difficult</th></tr> 
                 <tr><th>Data Structures</th>
                 <td><input id="ds_easy" type="number" min="0" max="3" class="knob"  data-width="90" data-height="90" data-fgColor="#00a65a"></td>
                 <td><input id="ds_medium" type="number" min="0" max="4" class="knob"  data-width="90" data-height="90" data-fgColor="#f39c12"></td>
                 <td><input id="ds_hard" type="number" min="0" max="3" class="knob"  data-width="90" data-height="90" data-fgColor="#f56954"></td>
                 </tr>
                 </table>         
                
                 
                
                
             
<!-- Select multiple-->
           
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>
   
    <section class="content col-lg-6">
     
          <div class="box box-success" style="overflow-x:auto">
              
            <!-- /.box-header -->
            <div class="box-body">
              <table >
                 <tr><th></th><th >Basics</th><th>Web Applications</th><th>OOP principles</th></tr> 
                 <tr><th>Java</th>
                 <td><input id="java_basics" type="number" min="0" max="3" class="knob"  data-width="90" data-height="90" data-fgColor="#00a65a"></td>
                 <td><input id="java_web" type="number" min="0" max="4" class="knob"  data-width="90" data-height="90" data-fgColor="#f39c12"></td>
                 <td><input id="java_oop" type="number" min="0" max="3" class="knob"  data-width="90" data-height="90" data-fgColor="#f56954"></td>
                 </tr>
                 </table>
                 
                 <table>
                 <tr><th></th><th >Basics</th><th>Project Work</th></tr> 
                 <tr><th>Android</th>
                 <td><input id="droid_basics" type="number" min="0" max="5" class="knob"  data-width="90" data-height="90" data-fgColor="#00a65a"></td>
                 <td><input id="droid_projects" type="number" min="0" max="5" class="knob"  data-width="90" data-height="90" data-fgColor="#f39c12"></td>
                 </tr>
                 </table>
               <table  >
                
                 <tr><th >Understanding</th><th>Reasoning</th><th>Communication</th><th>Score</th></tr> 
                 <tr>
                 <td><input id="understanding" type="number" min="10" max="10" class="knob"  data-width="90" data-height="90" data-fgColor="#00a65a"></td>
                 <td><input id="reasoning" type="number" min="10" max="10" class="knob"  data-width="90" data-height="90" data-fgColor="#f39c12"></td>
                 <td><input id="communication" type="number" min="0" max="5" class="knob"  data-width="90" data-height="90" data-fgColor="#f56954"></td>
                 <td><input id="score" type="number" value="0" min="0" max="70" class="knob"  data-width="90" data-height="90" data-fgColor="blue"></td>
                 </tr>
                 
                 </table>  
				 <div class="form-group">
                  
                  <textarea class="form-control" id="videocall_comment"  rows="4" placeholder="Enter Your comments" style="resize:none"></textarea>
                </div>

					
            </div>
            <!-- /.box-body -->
          </div>
          

    </section>  

   
            </div>
      
             </div>
            </div>
     
      </div>
      <div class="modal-footer" style="margin-top:-20px">
   		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-warning" onclick="process('hold')" data-dismiss="modal">Hold</button>
        <button type="button" class="btn btn-danger" onclick="process('waitlist')" data-dismiss="modal">Waitlist</button>
        <button type="button" class="btn btn-success" onclick="process('shortlist')" data-dismiss="modal">Shortlist</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="videocallscheduler" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id="schedulevideocall" class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header " style="background-color:green">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color:white">Schedule Videocall</h4>
      </div>
      <div class="modal-body">
      <div class="box-body" >
      <div id="external-events">
                <%
                ArrayList<Recruiter> panelist_array=Query.getPanelists();
                Iterator<Recruiter>panelists=panelist_array.iterator();
                while(panelists.hasNext()){
                	Recruiter panel=panelists.next();
                	out.print("<div class='external-event bg-green' id='"+panel.getUsername()+"'>"+panel.getName()+"</div>");
                }
                
                %>
                
                
              </div>
            <div id="calendar"></div>
            </div>
     
      </div>
      <div class="modal-footer" style="margin-top:-20px">
   		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-warning" onclick="process('hold')" data-dismiss="modal">Hold</button>
        <button type="button" class="btn btn-danger" onclick="process('waitlist')" data-dismiss="modal">Waitlist</button>
        <button type="button" class="btn btn-success" onclick="process('shortlist')" data-dismiss="modal">Shortlist</button>
      </div>
    </div>
  </div>
</div>




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Screening
        <small>All candidates who have applied</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><% out.print(Query.getCandidateCount());%></h3>

              <p>Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><%out.print(Query.getApplications(5).size()); %></h3>

              <p>Shortlisted Candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-paper-airplane"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><%out.print(Query.getApplications(3).size()); %></h3>

              <p>Pending Candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><%out.print(Query.getApplications(4).size()); %></h3>

              <p>Waitlisted Candidates</p>
            </div>
            <div class="icon">
              <i class="ion ion-trash-a"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      
      <div class="row">
        <div class="col-xs-12" >
        <div class="box box-warning" >
            <div class="box-header">
              <h3 class="box-title">Pending</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"  >
              <table id="pending" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <tr>
                  <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Panelist</th>
                  <th>Schedule</th>
                  <th>Action</th>
                </tr>
                
                </thead>
                <tbody><%
                   int id=1;
                 Iterator<Application>Pending=Query.getApplications(3).listIterator();
                 
                 while(Pending.hasNext()){
                	
                	 Application current=Pending.next();
                	 out.print("<tr>");
                	 out.print("<td>"+current.getInternship_name()+"</td>");
                	 out.print("<td id='username_"+id+"'>"+current.getApplicant_name()+"</td>");
                	 out.print("<td id='education_"+id+"'>"+current.getApplicant_education()+"</td>");
                	 out.print("<td id='university_"+id+"'>"+current.getApplicant_college()+"</td>");
                	 out.print("<td id='contact_"+id+"'>"+current.getApplicant_contact()+"</td>");
                	 out.print("<td id='email_"+id+"'>"+current.getApplicant_email()+"</td>");
                	 out.print("<td id='comment_"+id+"'>"+current.getRound2_comment()+"</td>");
                	 out.print("<td id='panelist_"+id+"'>"+current.getRound2_panelist()+"</td>");
                	 out.print("<td id='schedule_"+id+"'>"+new SimpleDateFormat().format(current.getRound2_date())+"</td>");
                	 out.print("<td>"+"<button title=\"Make videocall\" type=\"button\" data-toggle=\"modal\" data-target=\"#videocall\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-video-camera\"></a></button>"
                			 +"<button title=\"Schedule videocall\" type=\"button\" data-toggle=\"modal\" data-target=\"#videocallscheduler\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-calendar-plus-o\"></a></button>"			
                			 +"    <button title=\"Resume\" type=\"button\" onclick=\"window.open('uploads/"+current.getApplicant_email()+".pdf','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;\"  ><a class=\"fa fa-file-pdf-o\" ></a></button>"
                			 			+"</a></td>");
                	 out.print("</tr>");
                	 id++;
                 }
                 %>
                 </tbody>
                <tfoot>
                <tr>
                 <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
          <!-- /.box -->
      
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Shortlisted</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
              <table id="shortlisted" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <tr>
                  <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                
                </thead>
                <tbody>
                
                   <%
                   
                 Iterator<Application>Shortlisted=Query.getApplications(5).listIterator();
                 while(Shortlisted.hasNext()){
                	 Application current=Shortlisted.next();
                	 out.print("<tr>");
                	 out.print("<td>"+current.getInternship_name()+"</td>");
                	 out.print("<td id='username_"+id+"'>"+current.getApplicant_name()+"</td>");
                	 out.print("<td id='education_"+id+"'>"+current.getApplicant_education()+"</td>");
                	 out.print("<td id='university_"+id+"'>"+current.getApplicant_college()+"</td>");
                	 out.print("<td id='contact_"+id+"'>"+current.getApplicant_contact()+"</td>");
                	 out.print("<td id='email_"+id+"'>"+current.getApplicant_email()+"</td>");
                	 out.print("<td id='comment_"+id+"'>"+current.getRound1_comment()+"</td>");
                	 out.print("<td>"+"<button title=\"Reconsider\" type=\"button\" data-toggle=\"modal\" data-target=\"#videocall\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-anchor\"></a></button>"
     			 			+"<button title=\"Resume\" type=\"button\" onclick=\"window.open('uploads/"+current.getApplicant_email()+".pdf','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;\"  ><a class=\"fa fa-file-pdf-o\" ></a></button>"
     			 			+"</a></td>");
                	 out.print("</tr>");
                	 id++;
                	
                 }
                 %>      
                </tbody>
                <tfoot>
                <tr>
                 <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
          <!-- /.box -->
      
      <div class="row">
        <div class="col-xs-12">
        <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Waitlisted</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
              <table id="waitlisted" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <tr>
                  <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Panelist</th>
                  <th>Schedule</th>
                  <th>Action</th>
                </tr>
                
                </thead>
                <tbody>
               
                   <%
                 Iterator<Application>Waitlisted=Query.getApplications(4).listIterator();
                 while(Waitlisted.hasNext()){
                	 Application current=Waitlisted.next();
                	 out.print("<tr>");
                	 out.print("<td>"+current.getInternship_name()+"</td>");
                	 out.print("<td id='username_"+id+"'>"+current.getApplicant_name()+"</td>");
                	 out.print("<td id='education_"+id+"'>"+current.getApplicant_education()+"</td>");
                	 out.print("<td id='university_"+id+"'>"+current.getApplicant_college()+"</td>");
                	 out.print("<td id='contact_"+id+"'>"+current.getApplicant_contact()+"</td>");
                	 out.print("<td id='email_"+id+"'>"+current.getApplicant_email()+"</td>");
                	 out.print("<td id='comment_"+id+"'>"+current.getRound1_comment()+"</td>");
                	 out.print("<td id='panelist_"+id+"'>"+current.getRound2_panelist()+"</td>");
                	 out.print("<td id='schedule_"+id+"'>"+current.getRound2_date()+"</td>");
                	 out.print("<td>"+"<button title=\"Reconsider\" type=\"button\" data-toggle=\"modal\" data-target=\"#videocall\" data-candidate=\""+current.getApplicant_email()+"\" data-internship=\""+current.getInternship_id()+"\" data-id=\""+id+"\"><a  class=\"fa  fa-anchor\"></a></button>"
                			 			+"    <button title=\"Resume\" type=\"button\" onclick=\"window.open('uploads/"+current.getApplicant_email()+".pdf','height=400,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;\"  ><a class=\"fa fa-file-pdf-o\" ></a></button>"
                			 			+"</a></td>");
                	 
                	 out.print("</tr>");
                	 id++;
                 }
                 %>  
                </tbody>
                <tfoot>
                <tr>
                 <th>Internship</th>
                  <th>Applicant Name</th>
                  <th>Education</th>
                  <th>College</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Comment</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
          <!-- /.box -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   </div>
  <jsp:include page="recruiter-footer.jsp"></jsp:include>
    
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<!-- datepicker -->
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>

<script>
 var internship_id,candidate;
 var panelists=document.getElementById('external-events').innerHTML;
 $(":input").bind('keyup mouseup',function(){update_score();});
 function update_score(){
	 
	 var total=0;
	 $('.knob').each(function(i, obj) {
		
		    if(obj.id!="score"){total=total+parseInt(obj.value);}
		});
	
	 $('#score').val(total).trigger('change');
	 
 }
 $('#videocall').on('show.bs.modal', function(event) { 
	 var button = $(event.relatedTarget);
	
	document.getElementById("videocall_username").innerHTML=document.getElementById("username_"+button.data('id')).innerHTML;
	document.getElementById("videocall_contact").innerHTML=document.getElementById("contact_"+button.data('id')).innerHTML;
	document.getElementById("videocall_contact").href="tel://"+document.getElementById("contact_"+button.data('id')).innerHTML;
	document.getElementById("videocall_education").innerHTML=document.getElementById("education_"+button.data('id')).innerHTML;
	document.getElementById("videocall_university").innerHTML=document.getElementById("university_"+button.data('id')).innerHTML;
	document.getElementById("videocall_comment").innerHTML=document.getElementById("comment_"+button.data('id')).innerHTML;
	internship_id=button.data('internship');
	candidate=button.data('candidate');
	
	 var xmlhttp=new XMLHttpRequest();

	    xmlhttp.onreadystatechange=function() {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	      	
	var detail=JSON.parse(xmlhttp.responseText);
	document.getElementById("videocall_comment").value=detail.round2_comment;
	document.getElementById("videocall_email").innerHTML=detail.email;
	document.getElementById("algo_easy").value=detail.algo_easy;
	document.getElementById("algo_medium").value=detail.algo_medium;
	document.getElementById("algo_hard").value=detail.algo_hard;
	document.getElementById("ds_easy").value=detail.ds_easy;
	document.getElementById("ds_medium").value=detail.ds_medium;
	document.getElementById("ds_hard").value=detail.ds_hard;
	document.getElementById("java_basics").value=detail.java_basics;
	document.getElementById("java_web").value=detail.java_web;
	document.getElementById("java_oop").value=detail.java_oop;
	document.getElementById("droid_basics").value=detail.droid_basics;
	document.getElementById("droid_projects").value=detail.droid_projects;
	document.getElementById("understanding").value=detail.understanding;
	document.getElementById("reasoning").value=detail.reasoning;
	document.getElementById("communication").value=detail.communication;
	document.getElementById("score").value=detail.round2_score;
	document.getElementById("videocall_resume").href="uploads/"+candidate+".pdf";
	document.getElementById("hangout").innerHTML=' <div class="g-hangout" data-render="createhangout" id="videocall_hangout"> </div>';
	document.getElementById("videocall_hangout").invites="[{ id : '"+detail.email+"', invite_type : 'EMAIL' }]";
	var fileref=document.createElement('script');
    fileref.setAttribute("type","text/javascript")
    fileref.setAttribute("src", "https://apis.google.com/js/platform.js");
      var head = document.getElementsByTagName('head')[0];
    head.appendChild(fileref);
   
    update_score();
	        }	
	    }
	    xmlhttp.open("POST","RetrieveApplication",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		
		xmlhttp.send("round=1"+"&applicant="+candidate+"&internship_id="+internship_id);
	 

	    update_score();
 });
 
 function process(action){
	 update_score();
		 
		var xmlhttp=new XMLHttpRequest();
		var round2_comment=document.getElementById("videocall_comment").value;
		var email=document.getElementById("videocall_email").innerHTML;
		var algo_easy=document.getElementById("algo_easy").value;
		var	algo_medium=document.getElementById("algo_medium").value;
		var	algo_hard=document.getElementById("algo_hard").value;
		var	ds_easy=document.getElementById("ds_easy").value;
		var	ds_medium=document.getElementById("ds_medium").value;
		var	ds_hard=document.getElementById("ds_hard").value;
		var	java_basics=document.getElementById("java_basics").value;
		var	java_web=document.getElementById("java_web").value;
		var	java_oop=document.getElementById("java_oop").value;
		var	droid_basics=document.getElementById("droid_basics").value;
		var	droid_projects=document.getElementById("droid_projects").value;
		var	understanding=document.getElementById("understanding").value;
		var	reasoning=document.getElementById("reasoning").value;
		var	score=document.getElementById("score").value;
		var	communication=document.getElementById("communication").value;
		    xmlhttp.onreadystatechange=function() {
		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		      	window.location.reload();
		   
		        }	
		    }
		    xmlhttp.open("POST","ProcessApplication",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("round=2&action="+action+"&comment="+round2_comment+"&internship_id="+internship_id+"&applicant="+candidate

					+"&algo_easy="+algo_easy
					+"&algo_medium="+ algo_medium
					+"&algo_hard="+ algo_hard
					+"&ds_easy="+ ds_easy
					+"&ds_medium="+ ds_medium
					+"&ds_hard="+ ds_hard
					+"&java_basics="+ java_basics
					+"&java_web="+ java_web
					+"&java_oop="+ java_oop
					+"&droid_basics="+ droid_basics
					+"&droid_projects="+ droid_projects
					+"&understanding="+ understanding
					+"&reasoning="+ reasoning
					+"&communication="+ communication
					+"&score="+ score
			);
		 
 
 }
 var calendar=false;
 $('#videocall').on('hide.bs.modal', function() { 
	internship_id=0;   
	candidate="";
	
	});
 $('#videocallscheduler').on('hide.bs.modal', function() { 
	 $('#videocallscheduler').fullCalendar('destroy');
		
		});
 $('#videocallscheduler').on('shown.bs.modal', function() { 

	   
	if(!calendar) {  $('#calendar').fullCalendar({
	      header: {
	        left: 'prev,next today',
	        center: 'title',
	        right: 'month,agendaWeek,agendaDay'
	      },
	      buttonText: {
	        today: 'today',
	        month: 'month',
	        week: 'week',
	        day: 'day'
	      },
	      //Random default events
	      events: [   ],
	      editable: false,
	      droppable: true, // this allows things to be dropped onto the calendar !!!
	      drop: function (date, allDay) { // this function is called when something is dropped

	        // retrieve the dropped element's stored Event Object
	        var originalEventObject = $(this).data('eventObject');
	        document.getElementById("external-events").innerHTML="";
	        
	        // we need to copy it, so that multiple events don't have a reference to the same object
	        var copiedEventObject = $.extend({}, originalEventObject);

	        // assign it the date that was reported
	        copiedEventObject.start = date;
	        copiedEventObject.allDay = allDay;
	        copiedEventObject.backgroundColor = $(this).css("background-color");
	        copiedEventObject.borderColor = $(this).css("border-color");

	        // render the event on the calendar
	        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
	        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
	        scheduled_event=copiedEventObject;
	        
	        // is the "remove after drop" checkbox checked?
	          // if so, remove the element from the "Draggable Events" list
	          $(this).remove();
	        

	      }
	    });
	    $('#calendar').fullCalendar('addEventSource','GetAllEvents');
	    /* ADDING EVENTS */
	    var currColor = "#3c8dbc"; //Red by default
	    //Color chooser button
	    var colorChooser = $("#color-chooser-btn");
	    $("#color-chooser > li > a").click(function (e) {
	      e.preventDefault();
	      //Save color
	      currColor = $(this).css("color");
	      //Add color effect to button
	      $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
	    });
	
	} calendar=true;
	document.getElementById('external-events').innerHTML=panelists;
	 ini_events($('#external-events div.external-event'));
		});

 
 var scheduled_event;
 function ini_events(ele) {
     ele.each(function () {

       // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
       // it doesn't need to have a start or end
       var eventObject = {
         title: $.trim($(this).text())+"-", // use the element's text as the event title
         id : $(this).attr('id')
       };
		
       // store the Event Object in the DOM element so we can get to it later
       $(this).data('eventObject', eventObject);

       // make the event draggable using jQuery UI
       $(this).draggable({
         zIndex: 1070,
         revert: true, // will cause the event to go back to its
         revertDuration: 0  //  original position after the drag
       });

     });
   }
 
 $(function () {
	
	   
	   
	  
	  $("#ds_easy,#java_basics,#java_oop,#ds_hard").knob({
		     
	       max:3,
	      draw: function () {
	    	  this.max=3;
	    	  
	      },change: function (value) {
	            update_score();
	            
	        }
	    
	    });
	  $("#ds_medium,#java_web").knob({
		     
	       max:4,
	      draw: function () {
	    	  this.max=4;
	    	  
	      }
	  ,change: function (value) {
          update_score();
      }
	    
	    });
	  $("#algo_easy,#algo_medium,#algo_hard,#droid_basics,#droid_projects,#communication").knob({
		     
	       max:5,
	      draw: function () {
	    	  this.max=5;
	    	  
	      },change: function (value) {
	            update_score();
	        }
	    
	    });
	  $("#understanding,#reasoning").knob({
		     
	       max:10,
	      draw: function () {
	    	  this.max=10;
	    	  
	      },change: function (value) {
	            update_score();
	        }
	    
	    });
	 

	  $("#score").knob({
		     
	       max:70,
	       'dynamicDraw': true,
	      draw: function () {
	    	  this.val(this.value);
	    	
	      }
	    
	    });
    $("#pending").DataTable({
    	"scrollX": true,
        
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
    $("#shortlisted").DataTable({
    	"scrollX": true,
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
    $("#waitlisted").DataTable({
    	"scrollX": true,
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
   
  } );
  

  
 </script>
</body>
</html>
