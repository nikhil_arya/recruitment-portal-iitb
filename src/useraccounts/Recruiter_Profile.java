package useraccounts;

import java.util.Date;

public class Recruiter_Profile {
private String name,post,username;
private java.util.Date joining;
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

/**
 * @return the post
 */
public String getPost() {
	return post;
}

/**
 * @param post the post to set
 */
public void setPost(String post) {
	this.post = post;
}

/**
 * @return the joining
 */
public Date getJoining() {
	return joining;
}

/**
 * @param date the joining to set
 */
public void setJoining(java.util.Date date) {
	this.joining = date;
}

/**
 * @return the username
 */
public String getUsername() {
	return username;
}

/**
 * @param username the username to set
 */
public void setUsername(String username) {
	this.username = username;
}



}
