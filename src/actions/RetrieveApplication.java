package actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Authenticator.RequestorType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import database.Query;
import internships.Application;
import internships.TechnicalApplication;

/**
 * Servlet implementation class RetrieveApplication
 */
@WebServlet("/RetrieveApplication")
public class RetrieveApplication extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrieveApplication() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("round").equals("1")){
			TechnicalApplication Tech_Application = Query.getTechnicalApplication(request.getParameter("applicant"), Integer.parseInt(request.getParameter("internship_id")));
			Application Application=Query.getApplicationDetail(request.getParameter("applicant"), Integer.parseInt(request.getParameter("internship_id")));
			JSONObject detail=new JSONObject();
			try {
				detail.put("prof_c", Tech_Application.getProf_c());
				detail.put("prof_java", Tech_Application.getProf_java());
				detail.put("prof_ee", Tech_Application.getProf_ee());
				detail.put("prof_js", Tech_Application.getProf_js());
				detail.put("prof_html", Tech_Application.getProf_html());
				detail.put("prof_db", Tech_Application.getProf_db());
				detail.put("prof_droid", Tech_Application.getProf_droid());
				detail.put("skills", Tech_Application.getSkills());
				detail.put("available", Tech_Application.getAvail());
				detail.put("duration", Tech_Application.getRange_start()+" - "+Tech_Application.getRange_end());
				detail.put("question1", Tech_Application.getQuestion1());
				detail.put("question2", Tech_Application.getQuestion2());
				detail.put("question3", Tech_Application.getQuestion3());
				detail.put("round1_comment", Application.getRound1_comment());
				detail.put("round2_panelist", Application.getRound2_panelist());
				detail.put("round2_comment", Application.getRound2_comment());
				detail.put("round3_panelist", Application.getRound3_panelist());
				detail.put("round3_comment", Application.getRound3_comment());
				detail.put("email", Application.getApplicant_email());
				detail.put("algo_easy", Tech_Application.getAlgo_easy());
				detail.put("algo_medium", Tech_Application.getAlgo_medium());
				detail.put("algo_hard", Tech_Application.getAlgo_hard());
				detail.put("ds_easy", Tech_Application.getDs_easy());
				detail.put("ds_medium", Tech_Application.getDs_medium());
				detail.put("ds_hard",Tech_Application.getDs_hard());
				detail.put("java_basics",Tech_Application.getJava_basics());
				detail.put("java_web",Tech_Application.getJava_web());
				detail.put("java_oop",Tech_Application.getJava_oop());
				detail.put("droid_basics",Tech_Application.getDroid_basics());
				detail.put("droid_projects",Tech_Application.getDroid_projects());
				detail.put("understanding",Tech_Application.getUnderstanding());
				detail.put("reasoning",Tech_Application.getReasoning());
				detail.put("communication",Tech_Application.getCommunication());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PrintWriter out=response.getWriter();
			out.println(detail.toString());
		}
	}

}
