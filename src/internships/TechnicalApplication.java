package internships;

import java.util.Date;

public class TechnicalApplication {
private String prof_c,prof_html,prof_java,prof_ee,prof_js,prof_db,prof_droid,
				skills,avail,question1,question2,question3,username;
private Date range_start,range_end;
private int status,intersnhip_id;
private int algo_easy,algo_medium,algo_hard,ds_easy,ds_medium,ds_hard,java_basics,java_web,java_oop,droid_basics,droid_projects,understanding,reasoning,communication;
private int round2_score;
//

/**
 * @return the prof_c
 */
public String getProf_c() {
	return prof_c;
}

/**
 * @param prof_c the prof_c to set
 */
public void setProf_c(String prof_c) {
	this.prof_c = prof_c;
}

/**
 * @return the prof_html
 */
public String getProf_html() {
	return prof_html;
}

/**
 * @param prof_html the prof_html to set
 */
public void setProf_html(String prof_html) {
	this.prof_html = prof_html;
}

/**
 * @return the prof_java
 */
public String getProf_java() {
	return prof_java;
}

/**
 * @param prof_java the prof_java to set
 */
public void setProf_java(String prof_java) {
	this.prof_java = prof_java;
}

/**
 * @return the prof_ee
 */
public String getProf_ee() {
	return prof_ee;
}

/**
 * @param prof_ee the prof_ee to set
 */
public void setProf_ee(String prof_ee) {
	this.prof_ee = prof_ee;
}

/**
 * @return the prof_js
 */
public String getProf_js() {
	return prof_js;
}

/**
 * @param prof_js the prof_js to set
 */
public void setProf_js(String prof_js) {
	this.prof_js = prof_js;
}

/**
 * @return the skills
 */
public String getSkills() {
	return skills;
}

/**
 * @param skills the skills to set
 */
public void setSkills(String skills) {
	this.skills = skills;
}


/**
 * @return the avail
 */
public String getAvail() {
	return avail;
}

/**
 * @param avail the avail to set
 */
public void setAvail(String avail) {
	this.avail = avail;
}

/**
 * @return the question1
 */
public String getQuestion1() {
	return question1;
}

/**
 * @param question1 the question1 to set
 */
public void setQuestion1(String question1) {
	this.question1 = question1;
}

/**
 * @return the question2
 */
public String getQuestion2() {
	return question2;
}

/**
 * @param question2 the question2 to set
 */
public void setQuestion2(String question2) {
	this.question2 = question2;
}

/**
 * @return the question3
 */
public String getQuestion3() {
	return question3;
}

/**
 * @param question3 the question3 to set
 */
public void setQuestion3(String question3) {
	this.question3 = question3;
}

/**
 * @return the username
 */
public String getUsername() {
	return username;
}

/**
 * @param username the username to set
 */
public void setUsername(String username) {
	this.username = username;
}

/**
 * @return the intersnhip_id
 */
public int getIntersnhip_id() {
	return intersnhip_id;
}

/**
 * @param intersnhip_id the intersnhip_id to set
 */
public void setIntersnhip_id(int intersnhip_id) {
	this.intersnhip_id = intersnhip_id;
}

/**
 * @return the range_end
 */
public Date getRange_end() {
	return range_end;
}

/**
 * @param range_end the range_end to set
 */
public void setRange_end(Date range_end) {
	this.range_end = range_end;
}

/**
 * @return the range_start
 */
public Date getRange_start() {
	return range_start;
}

/**
 * @param range_start the range_start to set
 */
public void setRange_start(Date range_start) {
	this.range_start = range_start;
}

/**
 * @return the prof_db
 */
public String getProf_db() {
	return prof_db;
}

/**
 * @param prof_db the prof_db to set
 */
public void setProf_db(String prof_db) {
	this.prof_db = prof_db;
}

/**
 * @return the prof_droid
 */
public String getProf_droid() {
	return prof_droid;
}

/**
 * @param prof_droid the prof_droid to set
 */
public void setProf_droid(String prof_droid) {
	this.prof_droid = prof_droid;
}

/**
 * @return the status
 */
public int getStatus() {
	return status;
}

/**
 * @param status the status to set
 */
public void setStatus(int status) {
	this.status = status;
}

/**
 * @return the algo_easy
 */
public int getAlgo_easy() {
	return algo_easy;
}

/**
 * @param algo_easy the algo_easy to set
 */
public void setAlgo_easy(int algo_easy) {
	this.algo_easy = algo_easy;
}

/**
 * @return the algo_medium
 */
public int getAlgo_medium() {
	return algo_medium;
}

/**
 * @param algo_medium the algo_medium to set
 */
public void setAlgo_medium(int algo_medium) {
	this.algo_medium = algo_medium;
}

/**
 * @return the algo_hard
 */
public int getAlgo_hard() {
	return algo_hard;
}

/**
 * @param algo_hard the algo_hard to set
 */
public void setAlgo_hard(int algo_hard) {
	this.algo_hard = algo_hard;
}

/**
 * @return the ds_easy
 */
public int getDs_easy() {
	return ds_easy;
}

/**
 * @param ds_easy the ds_easy to set
 */
public void setDs_easy(int ds_easy) {
	this.ds_easy = ds_easy;
}

/**
 * @return the ds_medium
 */
public int getDs_medium() {
	return ds_medium;
}

/**
 * @param ds_medium the ds_medium to set
 */
public void setDs_medium(int ds_medium) {
	this.ds_medium = ds_medium;
}

/**
 * @return the ds_hard
 */
public int getDs_hard() {
	return ds_hard;
}

/**
 * @param ds_hard the ds_hard to set
 */
public void setDs_hard(int ds_hard) {
	this.ds_hard = ds_hard;
}

/**
 * @return the java_basics
 */
public int getJava_basics() {
	return java_basics;
}

/**
 * @param java_basics the java_basics to set
 */
public void setJava_basics(int java_basics) {
	this.java_basics = java_basics;
}

/**
 * @return the java_web
 */
public int getJava_web() {
	return java_web;
}

/**
 * @param java_web the java_web to set
 */
public void setJava_web(int java_web) {
	this.java_web = java_web;
}

/**
 * @return the java_oop
 */
public int getJava_oop() {
	return java_oop;
}

/**
 * @param java_oop the java_oop to set
 */
public void setJava_oop(int java_oop) {
	this.java_oop = java_oop;
}

/**
 * @return the droid_basics
 */
public int getDroid_basics() {
	return droid_basics;
}

/**
 * @param droid_basics the droid_basics to set
 */
public void setDroid_basics(int droid_basics) {
	this.droid_basics = droid_basics;
}

/**
 * @return the droid_projects
 */
public int getDroid_projects() {
	return droid_projects;
}

/**
 * @param droid_projects the droid_projects to set
 */
public void setDroid_projects(int droid_projects) {
	this.droid_projects = droid_projects;
}

/**
 * @return the understanding
 */
public int getUnderstanding() {
	return understanding;
}

/**
 * @param understanding the understanding to set
 */
public void setUnderstanding(int understanding) {
	this.understanding = understanding;
}

/**
 * @return the reasoning
 */
public int getReasoning() {
	return reasoning;
}

/**
 * @param reasoning the reasoning to set
 */
public void setReasoning(int reasoning) {
	this.reasoning = reasoning;
}

/**
 * @return the communication
 */
public int getCommunication() {
	return communication;
}

/**
 * @param communication the communication to set
 */
public void setCommunication(int communication) {
	this.communication = communication;
}

/**
 * @return the round2_score
 */
public int getRound2_score() {
	return round2_score;
}

/**
 * @param round2_score the round2_score to set
 */
public void setRound2_score(int round2_score) {
	this.round2_score = round2_score;
}

}
