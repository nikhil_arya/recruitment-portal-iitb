package internships;

import java.util.Date;

public class Internship {
private String title,category,status,about,eligibility,information;
private int id,duration,applicants,seats,hired;
private Date opening,closing;
/**
 * @return the opening
 */
public Date getOpening() {
	return opening;
}
/**
 * @param opening the opening to set
 */
public void setOpening(Date opening) {
	this.opening = opening;
}
/**
 * @return the hired
 */
public int getHired() {
	return hired;
}
/**
 * @param hired the hired to set
 */
public void setHired(int hired) {
	this.hired = hired;
}
/**
 * @return the seats
 */
public int getSeats() {
	return seats;
}
/**
 * @param seats the seats to set
 */
public void setSeats(int seats) {
	this.seats = seats;
}
/**
 * @return the applicants
 */
public int getApplicants() {
	return applicants;
}
/**
 * @param applicants the applicants to set
 */
public void setApplicants(int applicants) {
	this.applicants = applicants;
}
/**
 * @return the closing
 */
public Date getClosing() {
	return closing;
}
/**
 * @param closing the closing to set
 */
public void setClosing(Date closing) {
	this.closing = closing;
}
/**
 * @return the duration
 */
public int getDuration() {
	return duration;
}
/**
 * @param duration the duration to set
 */
public void setDuration(int duration) {
	this.duration = duration;
}
/**
 * @return the category
 */
public String getCategory() {
	return category;
}
/**
 * @param category the category to set
 */
public void setCategory(String category) {
	this.category = category;
}
/**
 * @return the status
 */
public String getStatus() {
	return status;
}
/**
 * @param status the status to set
 */
public void setStatus(String status) {
	this.status = status;
}
/**
 * @return the title
 */
public String getTitle() {
	return title;
}
/**
 * @param title the title to set
 */
public void setTitle(String title) {
	this.title = title;
}
/**
 * @return the id
 */
public int getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(int id) {
	this.id = id;
}
/**
 * @return the about
 */
public String getAbout() {
	return about;
}
/**
 * @param about the about to set
 */
public void setAbout(String about) {
	this.about = about;
}
/**
 * @return the eligibility
 */
public String getEligibility() {
	return eligibility;
}
/**
 * @param eligibility the eligibility to set
 */
public void setEligibility(String eligibility) {
	this.eligibility = eligibility;
}
/**
 * @return the information
 */
public String getInformation() {
	return information;
}
/**
 * @param information the information to set
 */
public void setInformation(String information) {
	this.information = information;
}

}
