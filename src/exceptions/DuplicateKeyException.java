package exceptions;

public class DuplicateKeyException extends Exception{
String Locale="";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 public DuplicateKeyException(String Locale)
	    {  super(Locale);
	       this.Locale=Locale;
	       
	    } 
	    public String getLocale()
	    { 
	        return Locale; 
	    } 
}
