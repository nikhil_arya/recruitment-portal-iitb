package exceptions;

public class VerificationFailedException extends Exception{
String Locale="";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 public VerificationFailedException(String Locale)
	    {  super(Locale);
	       this.Locale=Locale;
	       
	    } 
	    public String getLocale()
	    { 
	        return Locale; 
	    } 
}
