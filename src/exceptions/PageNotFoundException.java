package exceptions;

public class PageNotFoundException extends Exception{
String Locale="";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 public PageNotFoundException(String Locale)
	    {  super(Locale);
	       this.Locale=Locale;
	       
	    } 
	    public String getLocale()
	    { 
	        return Locale; 
	    } 
}
